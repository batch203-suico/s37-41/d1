const jwt = require("jsonwebtoken");
const { getProfile } = require('./controllers/userControllers');

// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret key.
const secret = "CourseBookingAPI";

// [SECTION] JSON Web Tokens

	// - JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
	// - Information is kept secure through the use of the secret code
	// - Only the system that knows the secret code that can decode the encrypted information
	

// Token Creation 
/*
	Analogy:
		Pack the gift provided with a aprovided lock, which can only be using the secret code as the key.
*/
// The "user" parameter will contain the values of the user upon login.
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a JSON web token using the jwt's "sign method".
	// Syntax:
		// jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])

	return jwt.sign(data, secret, {});
}

// Token Verification
/*
- Analogy
	Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/

// Middleware function
module.exports.verify = (req, res, next) =>{
	console.log('Authentication running...');
	// The token is retrieved from the request header.
	let token = req.headers.authorization;

	// If token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization headers.
	if (token !== undefined){
		// res.send({message: "Token received!"})

		// The token sent is a type of "Bearer " which when received contains the "Bearer" as a prefix to the string. To remove the "Bearer " prefix we used the slice method to retrieve only the token.
		token = token.slice(7, token.length);

		console.log(token);

		// Vaidate the "token" using the "verify" method to decrypt the token using the secret code.
		// Syntax: jwt.verfiy(token, secretOrPrivateKey, [options/callBackFunction])

		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return res.send({auth: "Invalid token!"});
			}
			// If JWT is valid
			else{
				req.user = data;
				// Allows the application to proceed with the next middleware function/callback function in the route.
				next();
			}
		})
		/*
		* Verify Token
		* Grab User from DB
		* req.user = user from DB
		* mount anothermiddleware, to check isAdmin()
		* if admin, next(), else res.status(401).send();
		*/
	}
	else{
		res.send({message: "Auth failed. No token provided!"})
	}
}

// Token decryption
/*
	- Analogy
		Open the gift and get the content
*/
module.exports.decode = (token) =>{
	if(token !== undefined){
		token  = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data)=>{
			if(err){
				// If token is not valid
				return null;
			}
			else{
				// decode method is used to obtain the information form the JWT
				// Syntax: jwt.decode(token, [options]);
				// Returns an object with access to the "payload" property which contains the user information stored when the token is generated.
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	else{
		//If token does not exist
		return null
	}
}
